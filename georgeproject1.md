# WEB STACK IMPLEMENTATION (LAMP STACK) IN AWS

#### 1. I setup EC2 INSTANCE (UBUNTU). 
#### 2. launch a new EC2 instance of t2.micro family with Ubuntu Server 20.04 LTS (HVM)
#### 3. Connect via SSH and update/upgrade all the packages.
#### 4. Install Apache2 and run the hostname on the browser to confirm that apache2 index.html page displayed.
<img width="925" alt="APACHE" src="https://user-images.githubusercontent.com/89888370/133908400-3c98d43e-0578-4e64-b10f-0df8e5ca6e74.PNG">
#### 5  Install mysql 

<img width="454" alt="mysql logged in" src="https://user-images.githubusercontent.com/89888370/133908397-e6d5c311-006a-452a-93c8-03b413e93e4e.PNG">

<img width="454" alt="MYSQL INSTALLED" src="https://user-images.githubusercontent.com/89888370/133908396-8498a4dd-5f26-4133-8699-d4724e0e3af8.PNG">

#### 6  Install php and create projectlamp folder in /var/www/html/ folder.

<img width="453" alt="php script installed" src="https://user-images.githubusercontent.com/89888370/133908398-effce0da-f4d0-4853-8648-f6497e1c4704.PNG">

#### 7  Create php file in projectlamp folder. I created george.php and index.php for my testing 

<img width="936" alt="MY_php_file" src="https://user-images.githubusercontent.com/89888370/133908395-17318fcf-935f-47ac-afe5-76df62d66a98.PNG">

<img width="957" alt="LAMP WORKING" src="https://user-images.githubusercontent.com/89888370/133908401-5a304a0b-75e3-4479-8e42-2088bc2377bf.PNG">

<img width="949" alt="projectlamp_index_page" src="https://user-images.githubusercontent.com/89888370/133908399-8eda6b65-1c83-4dbd-9bd3-ee688e6386f1.PNG">

#### 8 run the path on a browser.


